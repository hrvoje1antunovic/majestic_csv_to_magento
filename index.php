<?php

use League\Csv\Reader;
use League\Csv\Writer;

require  'vendor/league/csv/autoload.php';

function checkPrevious($allRows, $offset){

    $previous = $allRows[$offset-1];
    $current = $allRows[$offset];
    echo '<br>offset: '.$offset.' sku: '.$current["UPC/SKU"].' prev - '.$previous['Material Master'] .' - current:'. $current['Material Master'];

    if($previous['Material Master'] === $current['Material Master']){
        return true;
    }
}
function checkNext($allRows, $offset){

    $previous = $allRows[$offset+1];
    $current = $allRows[$offset];
    echo '<br>offset: '.$offset.' sku: '.$current["UPC/SKU"].' prev - '.$previous['Material Master'] .' - current:'. $current['Material Master'];

    if($previous['Material Master'] === $current['Material Master']){
        return true;
    }
}

//read


$csv = $reader = Reader::createFromPath('majestic_test.csv');

$csv->setHeaderOffset(0);
$header_offset = $csv->getHeaderOffset();
$header = $csv->getHeader();

$records = $reader->getRecords();
$writer = Writer::createFromPath('file.csv', 'w+');


$child_products_counter = 0;

$allRows = $reader->jsonSerialize(1);

foreach ($allRows as $offset => $record) {


    if($offset>1){
        //from second row and on
        //check previous row if it's part of same configurable product
        //if yes, insert simple product

        if(checkPrevious($allRows, $offset)){

            echo '<br>catch</br>';
            $child_products_counter +=1;

            //previous is checked and it is part of same configurable product
            //inserting it as simple product row
            $child_product_visibility = 'Not Visible Individually';



            $row = array(
                'sku' => $record["UPC/SKU"] ,
                'configurable_variations' => '', //!!!
                'additional_attributes' => '', //!!!
                'update_attribute_set' => 1,
                'store_view_code' => 'default',
                'attribute_set_code' => 'Default',
                'product_type' => 'simple', //!!!
                'name' => $record['Short Description/Title'],
                'price'=> '', //!!!
                'categories' => '',
                'description' => $record["Long Description"],
                'weight' => '', //!!!
                'tax_class_name' => 'Taxable Goods',
                'visibility' => $child_product_visibility, //!!!,
                'color' => $record["Product Color"],
                'size' =>$record["Size"],
                'base_image' =>$record["Image 1"],
                'image' =>'',
                'small_image' => '',
                'thumbnail' => '',
                'additional_images' => '',
                'product_online' => 1,
                'meta_title' =>$record["Short Description/Title"],
                'es_supplier' => 'Majestic Glove',
                'qty' => 1,
                'is_in_stock' => 1,
                'decorations' => ''
            );
             $writer->insertOne($row);


        }else{
            //current row checked and compared with previous row
            //not part of same configurable product
            //if $child_products_counter is bigger than 0 then insert configurable row
            if($child_products_counter>0){
                $row = array(
                    'sku' => 'xxx'.$allRows[$offset-1]["UPC/SKU"].'_cfg',
                    'configurable_variations' => '', //!!!
                    'additional_attributes' => '', //!!!
                    'update_attribute_set' => 1,
                    'store_view_code' => 'default',
                    'attribute_set_code' => 'Default',
                    'product_type' => 'configurable', //!!!
                    'name' => $allRows[$offset-1]['Short Description/Title'],
                    'price'=> '', //!!!
                    'categories' => '',
                    'description' => $allRows[$offset-1]["Long Description"],
                    'weight' => '', //!!!
                    'tax_class_name' => 'Taxable Goods',
                    'visibility' => 'Catalog Search', //!!!,
                    'color' => '',
                    'size' =>'',
                    'base_image' =>$allRows[$offset-1]["Image 1"],
                    'image' =>'',
                    'small_image' => '',
                    'thumbnail' => '',
                    'additional_images' => '',
                    'product_online' => 1,
                    'meta_title' =>$allRows[$offset-1]["Short Description/Title"],
                    'es_supplier' => 'Majestic Glove',
                    'qty' => 1,
                    'is_in_stock' => 1,
                    'decorations' => ''
                );
                 $writer->insertOne($row);

                 $child_products_counter = 0;
                $child_product_visibility = 'Catalog, Search';

                if(checkNext($allRows, $offset)) {
                    $child_product_visibility = 'Not Visible Individually';
                }

                $row = array(
                    'sku' => $record["UPC/SKU"] ,
                    'configurable_variations' => '', //!!!
                    'additional_attributes' => '', //!!!
                    'update_attribute_set' => 1,
                    'store_view_code' => 'default',
                    'attribute_set_code' => 'Default',
                    'product_type' => 'simple', //!!!
                    'name' => $record['Short Description/Title'],
                    'price'=> '', //!!!
                    'categories' => '',
                    'description' => $record["Long Description"],
                    'weight' => '', //!!!
                    'tax_class_name' => 'Taxable Goods',
                    'visibility' => $child_product_visibility, //!!!,
                    'color' => $record["Product Color"],
                    'size' =>$record["Size"],
                    'base_image' =>$record["Image 1"],
                    'image' =>'',
                    'small_image' => '',
                    'thumbnail' => '',
                    'additional_images' => '',
                    'product_online' => 1,
                    'meta_title' =>$record["Short Description/Title"],
                    'es_supplier' => 'Majestic Glove',
                    'qty' => 1,
                    'is_in_stock' => 1,
                    'decorations' => ''
                );
                $writer->insertOne($row);

            }else{
                $child_product_visibility = 'Catalog, Search';

                if(checkNext($allRows, $offset)) {
                    $child_product_visibility = 'Not Visible Individually';
                }
                    $row = array(
                    'sku' => $record["UPC/SKU"] ,
                    'configurable_variations' => '', //!!!
                    'additional_attributes' => '', //!!!
                    'update_attribute_set' => 1,
                    'store_view_code' => 'default',
                    'attribute_set_code' => 'Default',
                    'product_type' => 'simple', //!!!
                    'name' => $record['Short Description/Title'],
                    'price'=> '', //!!!
                    'categories' => '',
                    'description' => $record["Long Description"],
                    'weight' => '', //!!!
                    'tax_class_name' => 'Taxable Goods',
                    'visibility' => $child_product_visibility, //!!!,
                    'color' => $record["Product Color"],
                    'size' =>$record["Size"],
                    'base_image' =>$record["Image 1"],
                    'image' =>'',
                    'small_image' => '',
                    'thumbnail' => '',
                    'additional_images' => '',
                    'product_online' => 1,
                    'meta_title' =>$record["Short Description/Title"],
                    'es_supplier' => 'Majestic Glove',
                    'qty' => 1,
                    'is_in_stock' => 1,
                    'decorations' => ''
                );
                $writer->insertOne($row);
            }
        }
   }



  /*echo 'Product Title: ';
    echo json_encode($record["Short Description/Title"]); echo '<br>';
    echo 'SKU: ';
    echo json_encode($record["UPC/SKU"]);echo '<br>';
    echo 'Size: ';
    echo json_encode();echo '<br>';
    echo 'Color: ';
    echo json_encode();echo '<br>';
    echo 'Supplier: ';
    echo json_encode($record["Manufacturer"]);echo '<br>';
    echo 'Description: ';
    echo json_encode($record["Long Description"]);echo '<br>';
    echo 'Bulleted Features: ';
    echo json_encode($record["Bulleted Features"]);echo '<br>';
    echo 'Product Category 1: ';
    echo json_encode($record["Product Category 1"]);echo '<br>';
    echo 'Product Category 2: ';
    echo json_encode($record["Product Category 2"]);echo '<br>';echo '<br>';*/

     //if($record['Material Master'] == $records[$offset]['Material Master'] ){
       // echo " 0 ".$record['UPC/SKU']." 1 ".$records[$offset+1]['UPC/SKU'];
 // }
 //var_export($record);


}


